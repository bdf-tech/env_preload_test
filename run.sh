export test_key='original'

echo '# Compiling...'
rm lib.so lib.o test &> /dev/null
gcc -fPIC -c lib.c -o lib.o
gcc -shared -o lib.so lib.o
gcc test.c -o test

echo ''
echo '# C Test non-hook...'
./test

echo ''
echo '# C Test hook...'
LD_PRELOAD="./lib.so" ./test


echo ''
echo '# PY Test non-hook...'
python3 test.py

echo ''
echo '# PY Test hook...'
LD_PRELOAD="./lib.so" python3 test.py
rm lib.so lib.o test &> /dev/null
